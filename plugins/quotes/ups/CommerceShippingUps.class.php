<?php

/**
 * UPS rate commerce shipping quote plugin.
 */
class CommerceShippingUps extends CommerceShippingQuote {

  public function settings_form(&$form, $rules_settings) {
    $currencies = commerce_currencies(TRUE);

    $form['shipping_address'] = array(
      '#type' => 'fieldset',
      '#title' => t('Enter The Ship From Address'),
      '#weight' => 98,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $form['shipping_address']['address1'] = array(
      '#type' => 'textfield',
      '#title' => 'Address1',
      '#default_value' => is_array($rules_settings) && isset($rules_settings['shipping_address']['address1']) ? $rules_settings['shipping_address']['address1'] : null,
      '#weight' => 10,
      '#size' => 40,
      '#required' => TRUE,
    );
    $form['shipping_address']['address2'] = array(
      '#type' => 'textfield',
      '#title' => 'Address1',
      '#default_value' => is_array($rules_settings) && isset($rules_settings['shipping_address']['address2']) ? $rules_settings['shipping_address']['address2'] : null,
      '#weight' => 11,
      '#size' => 40,
    );
    $form['shipping_address']['address3'] = array(
      '#type' => 'textfield',
      '#title' => 'Address3',
      '#default_value' => is_array($rules_settings) && isset($rules_settings['shipping_address']['address3']) ? $rules_settings['shipping_address']['address3'] : null,
      '#weight' => 12,
      '#size' => 40,
    );
    $form['shipping_address']['city'] = array(
      '#type' => 'textfield',
      '#title' => 'City',
      '#default_value' => is_array($rules_settings) && isset($rules_settings['shipping_address']['city']) ? $rules_settings['shipping_address']['city'] : null,
      '#weight' => 13,
      '#size' => 40,
      '#required' => TRUE,
    );
    $form['shipping_address']['state'] = array(
      '#type' => 'textfield',
      '#title' => 'State',
      '#default_value' => is_array($rules_settings) && isset($rules_settings['shipping_address']['state']) ? $rules_settings['shipping_address']['state'] : null,
      '#weight' => 14,
      '#size' => 2,
      '#maxlength' => 2,
      '#required' => TRUE,
    );
    $form['shipping_address']['postal_code'] = array(
      '#type' => 'textfield',
      '#title' => 'Postal Code',
      '#default_value' => is_array($rules_settings) && isset($rules_settings['shipping_address']['postal_code']) ? $rules_settings['shipping_address']['postal_code'] : null,
      '#weight' => 15,
      '#required' => TRUE,
      '#size' => 9,
      '#maxlength' => 9,
    );
    $form['shipping_address']['country'] = array(
      '#type' => 'textfield',
      '#title' => 'Country',
      '#default_value' => is_array($rules_settings) && isset($rules_settings['shipping_address']['country']) ? $rules_settings['shipping_address']['country'] : null,
      '#weight' => 16,
      '#size' => 2,
      '#maxlength' => 2,
      '#required' => TRUE,
    );

    $form['account_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Enter UPS account settings'),
      '#weight' => 99,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['account_settings']['access_license'] = array(
      '#type' => 'textfield',
      '#title' => 'UPS Access License',
      '#default_value' => is_array($rules_settings) && isset($rules_settings['account_settings']['access_license']) ? $rules_settings['account_settings']['access_license'] : null,
      '#weight' => 100,
      '#required' => TRUE,
    );
    $form['account_settings']['user_id'] = array(
      '#type' => 'textfield',
      '#title' => 'UPS UserID',
      '#default_value' => is_array($rules_settings) && isset($rules_settings['account_settings']['user_id']) ? $rules_settings['account_settings']['user_id'] : null,
      '#weight' => 101,
      '#required' => TRUE,
    );
    $form['account_settings']['password'] = array(
      '#type' => 'textfield',
      '#title' => 'UPS Password',
      '#default_value' => is_array($rules_settings) && isset($rules_settings['account_settings']['password']) ? $rules_settings['account_settings']['password'] : null,
      '#weight' => 102,
      '#required' => TRUE,
    );
    $form['account_settings']['shipper_account'] = array(
      '#type' => 'textfield',
      '#title' => 'UPS Shipper Account',
      '#default_value' => is_array($rules_settings) && isset($rules_settings['account_settings']['shipper_account']) ? $rules_settings['account_settings']['shipper_account'] : null,
      '#weight' => 103,
      '#required' => TRUE,
    );

    $form['shipping_methods'] = array(
      '#type' => 'fieldset',
      '#title' => 'Select available shipping methods',
      '#weight' => 106,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['shipping_methods']['methods'] = array(
      '#type' => 'checkboxes',
      '#options' => $this->commerce_shipping_ups_rate_names(),
      '#title' => t('Select available shipping options'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['shipping_methods']['methods']) ? $rules_settings['shipping_methods']['methods'] : "03",
    );

    $form['shipping_methods']['default'] = array(
      '#type' => 'select',
      '#title' => t('Select default shipping option'),
      '#options' => $this->commerce_shipping_ups_rate_names(),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['shipping_methods']['default']) ? $rules_settings['shipping_methods']['default'] : "03",
    );
  }

  public function submit_form($pane_values, $checkout_pane, $order = NULL) {
    $settings = $this->settings;
    if (empty($order)) {
      $order = $this->order;
    }
    $form = parent::submit_form($pane_values, $checkout_pane, $order);

    // Merge in values from the order.
    if (!empty($order->data['commerce_shipping_ups'])) {
      $pane_values += $order->data['commerce_shipping_ups'];
    }

    // Merge in default values.
    $pane_values += array(
      'method' => $settings['shipping_methods']['default'],
    );


    $form['method'] = array(
      '#type' => 'radios',
      '#title' => t('Select shipping method'),
      '#description' => t('Select your shipping type. Shipping rates will be calculated on the next page'),
      '#options' => $this->commerce_shipping_ups_rate_names(false),
      '#default_value' => $pane_values['method'],
      '#required' => TRUE,
    );

    return $form;
  }

  public function submit_form_validate($pane_form, $pane_values, $form_parents = array(), $order = NULL) {
    if (!$pane_values['method']) {
      form_set_error(implode('][', array_merge($form_parents, array('method'))), t('You must select a shipping method.'));
      return FALSE;
    }
  }

  public function calculate_quote($currency_code, $form_values = array(), $order = NULL, $pane_form = NULL, $pane_values = NULL) {
    $service = $form_values['method'];
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
    $order_wrapper->commerce_line_items;
    //instantiate and empty array for holding package meta data
    $package_builder = array();
    //loop through order line items to build shipping packages
    foreach ($order_wrapper->commerce_line_items as $index => $line_item_wrapper) {
      if ($line_item_wrapper->type->value() == 'product') {
        $quantity = $line_item_wrapper->quantity->value();
        $product_id = $line_item_wrapper->commerce_product->product_id->value();
        $product = commerce_product_load($product_id);
        if (is_array($product->commerce_package) && $product->commerce_package['und'][0]['shippable'] == 1) {
          $current_item = $product->commerce_package['und'][0];
          //generate a unique array key for each package size
          //(based on max quantity, length, width, and height of package)
          //ie: $package_builder[6-10-10-10];
          $package_key = $current_item['quantity'] . "-" . $current_item['length'] . "-" . $current_item['width'] . "-" . $current_item['height'];
          //set the package builder metadata from line item values
          if (!array_key_exists($package_key, $package_builder)) {
            //create the package_builder array for this size package if it hasn't been created yet
            $package_builder[$package_key] = array();
            $package_builder[$package_key]['weight'] = ((int) $quantity * $current_item['weight']);
            $package_builder[$package_key]['quantity'] = (int) $quantity;
          } else {
            $package_builder[$package_key]['weight']+= ( (int) $quantity * $current_item['weight']);
            $package_builder[$package_key]['quantity']+=(int) $quantity;
          }

          $package_builder[$package_key]['length'] = $current_item['length'];
          $package_builder[$package_key]['width'] = $current_item['width'];
          $package_builder[$package_key]['height'] = $current_item['height'];
          $package_builder[$package_key]['max_quantity'] = $current_item['quantity'];
        }
      }
    }
    $packages = array();
    //loop through the package metadata generated by the package builder array
    foreach ($package_builder as $package) {
      //if the package builder element has a greater quantity than it's max allowed quantity, split shippment into multiple packages
      if ($package['quantity'] > $package['max_quantity']) {
        $num_packages = ceil($package['quantity'] / $package['max_quantity']);
        $package_weight = $package['weight'] / $num_packages;
      } else {
        $num_packages = 1;
        $package_weight = $package['weight'];
      }
      //loop through num_packages and generate a new package object for each one
      for ($i = 1; $i <= $num_packages; $i++) {
        $packages[] = array(
          'description' => t('my description'),
          'weight' => round($package_weight),
          'code' => '02',
          'length' => $package['length'],
          'width' => $package['width'],
          'height' => $package['height'],
        );
      }
    }

    $amount = $this->commerce_shipping_ups_get_rates($packages, $service);
    $services = $this->commerce_shipping_ups_rate_names(true);
    $shipping_line_items = array();
    $shipping_line_items[] = array(
      'amount' => commerce_currency_decimal_to_amount($amount, $currency_code),
      'currency_code' => $currency_code,
      'label' => $services[$service],
    );
    return $shipping_line_items;
  }

  function commerce_shipping_ups_get_rates($packages=array(), $service='03', $testing_mode=1) {
    $settings = $this->settings;
    $order = $this->order;
    $customer_profile_id = $order->commerce_customer_shipping['und'][0]['profile_id'];
    $customer_profile = commerce_customer_profile_load($customer_profile_id);
    //trim the customer profile array for legibility
    $customer_profile = $customer_profile->commerce_customer_address['und'][0];

    if ($settings['account_settings']['access_license'] && $settings['account_settings']['user_id'] && $settings['account_settings']['password']) {
      require_once("classes/class.ups.php");
      require_once("classes/class.upsRate.php");
      $upsConnect = new ups($settings['account_settings']['access_license'], $settings['account_settings']['user_id'], $settings['account_settings']['password']);
      $upsConnect->setTestingMode($testing_mode);
      $upsConnect->debugMode = false;
      $module_path = drupal_get_path("module", "commerce_shipping_quotes");
      $upsConnect->setTemplatePath("./$module_path/plugins/quotes/ups/xml/");
      $upsRate = new upsRate($upsConnect);


      $upsRate->shipper(array('name' => 'mark',
        'shipperNumber' => $settings['account_settings']['shipper_account'],
        'address1' => $settings['shipping_address']['address1'],
        'address2' => $settings['shipping_address']['address2'],
        'address3' => $settings['shipping_address']['address3'],
        'city' => $settings['shipping_address']['city'],
        'state' => $settings['shipping_address']['state'],
        'postalCode' => $settings['shipping_address']['postal_code'],
        'country' => $settings['shipping_address']['country'],
        'phone' => null,
      )
      );

      $upsRate->shipTo(array('companyName' => 'mark',
        'attentionName' => $customer_profile['name_line'],
        'address1' => $customer_profile['thoroughfare'],
        'address2' => $customer_profile['premise'],
        'address3' => '',
        'city' => $customer_profile['locality'],
        'state' => $customer_profile['administrative_area'],
        'postalCode' => $customer_profile['postal_code'],
        'countryCode' => $customer_profile['country'],
        'phone' => null,
      )
      );

      foreach ($packages as $package) {
        $upsRate->package($package);
      }

      $upsRate->shipment(array('description' => 'my description', 'serviceType' => $service));

      //if packages have been created (shippable items in cart)
      if (is_array($packages)) {
        $upsRate->sendRateRequest();
        $rate = $upsRate->returnRate();

        if (array_key_exists('ERROR', $rate)) {
          //if there are packages for this order and and error was generated
          //then return user to the checkout page and tell them why
          drupal_set_message(t("There was an error calculating your shipping") . "<br />" . $rate['ERROR'], 'error');
          drupal_goto("checkout/" . $order->order_number);
        } else {
          return $rate["RATE"];
        }
      }
    }
  }

  function commerce_shipping_ups_rate_names($all = true) {
    $shipping_names = array(
      '01' => 'UPS Next Day Air',
      '02' => 'UPS Second Day Air',
      '03' => 'UPS Ground',
      '07' => 'UPS Worldwide Express',
      '08' => 'UPS Worldwide Expedited',
      '11' => 'UPS Standard',
      '12' => 'UPS Three-Day Select',
      '13' => 'Next Day Air Saver',
      '14' => 'UPS Next Day Air Early AM',
      '54' => 'UPS Worldwide Express Plus',
      '59' => 'UPS Second Day Air AM',
      '65' => 'UPS Saver',
    );
    if ($all == true) {
      return $shipping_names;
    } else {
      $settings = $this->settings;
      $shipping_names_filterd = array();
      foreach ($settings['shipping_methods']['methods'] as $key => $value) {
        if ($value > 0) {
          $shipping_names_filtered[$key] = $shipping_names[$value];
        }
      }
      return $shipping_names_filtered;
    }
  }

}

