<?php

/**
 * @file
 * Sets plugin variables
 */
$plugin = array(
  'title' => t('UPS'),
  'description' => t('Calculate shipping quotes from UPS.'),
  'handler' => array(
    'class' => 'CommerceShippingUps',
    'parent' => 'quote_base'
  ),
);
