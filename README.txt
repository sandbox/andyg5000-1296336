Commerce Shipping Quotes Module
=====================

This module provides shipping quote calculation for Drupal Commerce stores.

This module currently:

1. Provides shipping calculations from UPS based on package weight.

To configure:
-------------

1. Install and enable the module.
2. Enable UPS on the Shipping Settings page by visiting
   admin/commerce/config/shipping-methods.
3. Once enabled, click setting for the UPS plugin
4. Under the actions select edit for "Enable shipping method: UPS"
5. Complete the ship from address settings.
6. Complete the UPS account settings.
7. Follow the steps to setup the Commerce Package module
8. Shipping rates will be calculated after selecting the shipping method
   on the review order page.


Required Modules:
----------------

1. Commerce
2. Commerce Shipping
3. Commerce Package
4. Package Field


To Do:
----------------

1. Update the code so that it combines packages for multiple unique products.
   Currently it only combines packages for orders of multiple quantities of
   the same product.
2. Abstract the shipping address settings into a store profile entity using
   the addressfield module.
3. Develop plugins for USPS and FedEx
4. Develop a widget that uses Ajax to calculate the shipping on the cart and
   review pages.
