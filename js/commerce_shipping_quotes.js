function commerce_shipping_quotes_copy_shipping(checked,source,destination){
 
    var name, country, address1, address2, city, state, zip;
    if(checked==true){
        name = jQuery("#edit-customer-profile-"+source+"-commerce-customer-address-und-0-name-line").val();
        country = jQuery("#edit-customer-profile-"+source+"-commerce-customer-address-und-0-country").val();
        address1 = jQuery("#edit-customer-profile-"+source+"-commerce-customer-address-und-0-thoroughfare").val();
        address2 = jQuery("#edit-customer-profile-"+source+"-commerce-customer-address-und-0-premise").val();
        city = jQuery("#edit-customer-profile-"+source+"-commerce-customer-address-und-0-locality").val();
        state = jQuery("#edit-customer-profile-"+source+"-commerce-customer-address-und-0-administrative-area").val();
        zip = jQuery("#edit-customer-profile-"+source+"-commerce-customer-address-und-0-postal-code").val();
        jQuery("#edit-customer-profile-"+destination+"-commerce-customer-address-und-0-name-line").val(name);
        jQuery("#edit-customer-profile-"+destination+"-commerce-customer-address-und-0-country").val(country);
        jQuery("#edit-customer-profile-"+destination+"-commerce-customer-address-und-0-thoroughfare").val(address1);
        jQuery("#edit-customer-profile-"+destination+"-commerce-customer-address-und-0-premise").val(address2);
        jQuery("#edit-customer-profile-"+destination+"-commerce-customer-address-und-0-locality").val(city);
        jQuery("#edit-customer-profile-"+destination+"-commerce-customer-address-und-0-administrative-area").val(state);
        jQuery("#edit-customer-profile-"+destination+"-commerce-customer-address-und-0-postal-code").val(zip);

     }
    
}

function commerce_shipping_ups_calculate(){
    
}